package main

import (
	"Learngoproject/src/routes"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

func main() {
	router := gin.Default()
	routes.GithubRoutes(router)

	// create server
	err := router.Run(":" + os.Getenv("PORT"))
	if err != nil {
		log.Fatal("error")
	}
}
