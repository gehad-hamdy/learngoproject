package builders

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
)

type RequestBuilder struct {
	URL      string
	BODY     interface{}
	METHOD   string
	HEADERS  map[string]string
	PARAMS   map[string]string
	response interface{}
}

func NewRequestBuilder() *RequestBuilder {
	return &RequestBuilder{}
}

func (builder RequestBuilder) WithUrl(url string) RequestBuilder {
	builder.URL = url
	return builder
}
func (builder RequestBuilder) WithMethod(method string) RequestBuilder {
	builder.METHOD = method
	return builder
}
func (builder RequestBuilder) WithBodyData(body interface{}) RequestBuilder {
	builder.BODY = body
	return builder
}
func (builder RequestBuilder) WithHeaders(headers map[string]string) RequestBuilder {
	builder.HEADERS = headers
	return builder
}
func (builder RequestBuilder) WithQueryParams(params map[string]string) RequestBuilder {
	builder.PARAMS = params
	return builder
}
func (builder RequestBuilder) WithResponseModel(dst interface{}) RequestBuilder {
	builder.response = dst
	return builder
}
func (builder RequestBuilder) parseResponse() func(*http.Response) error {
	return func(resp *http.Response) error {
		return json.NewDecoder(resp.Body).Decode(builder.response)
	}
}
func (builder RequestBuilder) Request() error {
	if builder.METHOD == "GET" || builder.METHOD == "POST" || builder.METHOD == "PUT" || builder.METHOD == "DELETE" {
		body, _ := json.Marshal(builder.BODY)
		req, _ := http.NewRequest(builder.METHOD, builder.URL, bytes.NewBuffer(body))
		for key, value := range builder.HEADERS {
			req.Header.Set(key, value)
		}
		queryParams := req.URL.Query()
		for key, value := range builder.PARAMS {
			queryParams.Set(key, value)
		}
		req.URL.RawQuery = queryParams.Encode()
		dump, err := httputil.DumpRequest(req, true)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println(string(dump))
		}
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			return err
		}
		if builder.response != nil {
			parseErr := builder.parseResponse()(resp)
			if parseErr != nil {
				return parseErr
			}
		}
		defer resp.Body.Close()
		return nil
	} else {
		return errors.New("Supported methods are GET , POST , PUT , DELETE")
	}
}
