package responses

type GithubSearch struct {
	TotalCount  int `json:"total_count"`
	IncompleteResults bool `json:"incomplete_results"`
	Items []items `json:"items"`
}

type items struct {
	Id             int `json:"id"`
	NodeId         string `json:"node_id"`
	Name           string `json:"name"`
    FullName       string `json:"full_name"`
	Private        bool `json:"private"`
	DefaultBranch  string `json:"default_branch"`
	Fork           bool `json:"fork"`
	OpenIssues     int `json:"open_issues"`
	Watchers       int `json:"watchers"`
	Score          float64 `json:"score"`
	Owner          owner `json:"owner"`
}

type owner struct {
	Login         string `json:"login"`
	Id            int    `json:"id"`
	NodeId        string `json:"node_id"`
	AvatarUrl     string `json:"avatar_url"`
	Url           string `json:"url"`
	ReposUrl      string `json:"repos_url"`
	SiteAdmin     bool   `json:"site_admin"`
}
