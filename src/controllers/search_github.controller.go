package controllers

import (
	"Learngoproject/src/services"
	"github.com/gin-gonic/gin"
	"strconv"
)

type GithubController struct {
	githubService services.GithubService
}

func GetGithubController() *GithubController {
	controller := GithubController{}
	return &controller
}

func (controller GithubController) ListSearch(context *gin.Context)  {
	date := context.Query("date")
	sort := context.Query("sort")
	order := context.Query("order")
	page, _ := strconv.Atoi(context.Query("page"))

	// call the service
	resource := controller.githubService.ListSearch(date, sort, order, page)

	// return response
	context.JSON(resource.GetStatus(), resource.GetData())
}
