package requests

type SearchRequest struct {
	Created string  `json:"created"`
	Sort string `json:"sort"`
}
