package routes

import (
	"Learngoproject/src/controllers"
	"github.com/gin-gonic/gin"
)

func GithubRoutes(route *gin.Engine) {
	githubController := controllers.GetGithubController()
	search := route.Group("/github")
	search.GET("/search", githubController.ListSearch)
}
