package services

import (
	"Learngoproject/pkg/builders"
	"Learngoproject/src/repositories"
	"Learngoproject/src/resources"
	"Learngoproject/src/responses"
	"fmt"
)

type GithubService struct {
	repository repositories.GithubRepository
}

func GetGithubService() *GithubService {
	return &GithubService{}
}

func (service GithubService) ListSearch(date, sort, order string, page int) resources.IResource {
	response := new(responses.GithubSearch)
	path := fmt.Sprintf(`https://api.github.com/search/repositories?q=`)
	if date != "" {
		path += "created:>" + date
	}
	if sort != "" && date != "" {
		path += "&sort=" + sort
	} else if sort != "" && date == "" {
		path += "sort=" + sort
	}
	if order != "" && (date != "" || sort != "") {
		path += "&order=" + order
	} else if order != "" && (date == "" && sort == "") {
		path += "order=" + order
	}

	err := builders.NewRequestBuilder().
		WithMethod("GET").
		WithUrl(path).
		WithResponseModel(response).
		Request()

	if err != nil {
		return resources.GetError500Resource(err.Error())
	}

	return resources.GetSuccess200Resource(paginateData(response, page), "Success")
}

func paginateData(response *responses.GithubSearch, page int) *responses.GithubSearch {
	size := 100
	skip := (page - 1) * size
	if skip > len(response.Items) {
		skip = len(response.Items)
	}

	end := skip + size
	if end > len(response.Items) {
		end = len(response.Items)
	}

    response.Items = response.Items[skip:end]

	return response
}
